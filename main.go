package main

import (
	"fmt"
	"os"
)

func main() {
	if err := proverbse("proverbse.txt"); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func proverbse(name string) error {
	f, err := os.Create(name)
	if err != nil {
		return err
	}

	if _, err := fmt.Fprintln(f, "Errors are values"); err != nil {
		f.Close()
		return err
	}

	_, err = fmt.Fprintln(f, "Don't just check errors, handle them gracefully")
	f.Close()
	return err
}
